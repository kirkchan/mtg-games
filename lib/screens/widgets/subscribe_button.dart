import 'package:flutter/material.dart';
import 'package:mtg/screens/widgets/labeled_button.dart';

class SubscribeButton extends StatefulWidget {
  @override
  _SubscribeButtonState createState() => _SubscribeButtonState();
}

class _SubscribeButtonState extends State<SubscribeButton> {
  final Color color = Colors.lightBlue;
  final String label = 'SUBSCRIBE';

  final Function subscriptionService;
  final String id;

  IconData _icon;
  bool _isSubscribed = false;

  _SubscribeButtonState(
      {subscribed = false, this.subscriptionService, this.id}) {
    if (subscribed) {
      this._icon = Icons.star;
      this._isSubscribed = true;
    } else {
      this._icon = Icons.star_border;
    }
  }

  void _toggle() {
    _isSubscribed = !_isSubscribed;
    setState(() {
      if (_isSubscribed)
        this._icon = Icons.star;
      else
        this._icon = Icons.star_border;
    });
  }

  @override
  Widget build(BuildContext context) {
    return LabeledButton(
        color: this.color,
        icon: this._icon,
        label: this.label,
        function: _toggle);
  }
}
