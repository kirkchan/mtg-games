import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LabeledButton extends FlatButton {
  final Color color;
  final IconData icon;
  final String label;
  final Function function;

  const LabeledButton({this.color, this.icon, this.label, this.function});

  @override
  FlatButton build(BuildContext context) {
    return FlatButton(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: color,
              ),
            ),
          ),
        ],
      ), onPressed: () {
        print(this.label + ' pressed');

        if (this.function != null)
          this.function();
      },
    );
  }
}
