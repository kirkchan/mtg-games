import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mtg/data/models/location_item.dart';
import 'package:mtg/data/search_repository.dart';
import 'package:mtg/screens/store_view.dart';

class SearchView extends StatefulWidget {
  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  @override
  Widget build(BuildContext context) {
    List<LocationItem> locations = SearchRepository.loadResults(Category.all);

    Widget listItem(index) => new GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => StoreView(id: locations[index].id),
              ),
            );
          },
          child: Container(
            padding: const EdgeInsets.fromLTRB(20, 30, 20, 30),
            color: Colors.grey[100 * (index % 2 + 1)],
            child: Row(
              children: [
                Expanded(
                  /*1*/
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      /*2*/
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Text(
                          locations[index].name,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Text(
                        locations[index].cityDistrict,
                        style: TextStyle(
                          color: Colors.grey[500],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(locations[index].eventCount != null
                          ? (new Random().nextInt(200).toString())
                          : '0'),
                      Padding(
                        padding: const EdgeInsets.only(left: 4),
                        child: Icon(
                          locations[index].icon,
                          color: locations[index].iconColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          const SliverAppBar(
            pinned: true,
            expandedHeight: 175.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('Magic the Gathering'),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return listItem(index);
              },
              childCount: locations.length,
            ),
          ),
        ],
      ),
    );
  }
}
