import 'package:flutter/material.dart';
import 'package:mtg/data/location_repository.dart';
import 'package:mtg/data/models/event_item.dart';
import 'package:mtg/data/models/location.dart';
import 'package:mtg/screens/widgets/labeled_button.dart';
import 'package:mtg/screens/widgets/subscribe_button.dart';

class StoreView extends StatefulWidget {
  final int id;

  StoreView({this.id});

  @override
  _StoreViewState createState() => _StoreViewState(id: this.id);
}

class _StoreViewState extends State<StoreView> {
  final int id;

  _StoreViewState({this.id});

  @override
  Widget build(BuildContext context) {
    Location location = LocationRepostiory.loadLocation(this.id);

    List<Color> cardColors = [
      Colors.red[700].withOpacity(0.3),
      Colors.red[400].withOpacity(0.3),
      Colors.red[200].withOpacity(0.3),
      Colors.red[100].withOpacity(0.3),
    ];
    Widget eventsSection = CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          pinned: true,
          expandedHeight: 175.0,
          backgroundColor: Colors.white.withOpacity(0.0),
          flexibleSpace: FlexibleSpaceBar(
            title: Text(location.name),
            background: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: cardColors,
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                ),
              ),
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: cardColors,
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                ),
                child: Center(
                  child: Card(
                    color: Colors.white.withOpacity(0.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          trailing: Icon(Icons.access_time, color: Colors.red),
                          title: Text(location.eventList[index]?.name),
                          subtitle: Text(location.eventList[index]?.format),
                        ),
                        ButtonBar(
                          children: <Widget>[
                            FlatButton(
                              child: const Text(
                                'JOIN',
                                textAlign: TextAlign.end,
                              ),
                              textColor: Colors.white.withOpacity(0.7),
                              onPressed: () {
                                print('Joining game ' +
                                    location.eventList[index]?.id.toString());
                              },
                            ),
                            FlatButton(
                              child: const Text(
                                'INFO',
                                textAlign: TextAlign.end,
                              ),
                              textColor: Colors.white.withOpacity(0.7),
                              onPressed: () {
                                print('Query for game info ' +
                                    location.eventList[index]?.id.toString());
                              },
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
            childCount: location.eventList?.length,
          ),
        ),
      ],
    );

    if (location.eventList == null)
      return Scaffold(
        appBar: AppBar(title: Text(location.name)),
        body: Center(
          child: Text('No events'),
        ),
      );

    return Scaffold(
      body: eventsSection,
    );
  }
}
