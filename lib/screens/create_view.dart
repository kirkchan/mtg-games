import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CreateForm extends StatefulWidget {
  @override
  CreateFormState createState() {
    return CreateFormState();
  }
}

class CreateFormState extends State<CreateForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          ListTile(
            leading: const Icon(Icons.games),
            title: TextFormField(
              decoration: InputDecoration(
                hintText: "Game",
                border: InputBorder.none,
              ),
              enableSuggestions: true,
              
              validator: (value) {
                return (value.isEmpty) ? "Select game" : null;
              },
            ),
          ),
          ListTile(
            leading: const Icon(Icons.apps),
            title: TextFormField(
              decoration: InputDecoration(
                hintText: "Format",
                border: InputBorder.none,
              ),
              validator: (value) {
                return (value.isEmpty) ? "Select format" : null;
              },
            ),
          ),
          ListTile(
            leading: const Icon(Icons.location_on),
            title: TextFormField(
              decoration: InputDecoration(
                hintText: "Location",
                border: InputBorder.none,
              ),
              validator: (value) {
                return (value.isEmpty) ? "Select location" : null;
              },
              
            ),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  // Validate returns true if the form is valid, or false
                  // otherwise.
                  if (_formKey.currentState.validate()) {
                    // If the form is valid, display a Snackbar.
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Processing Data'),
                        duration: Duration(seconds: 1),
                      ),
                    );
                  }
                },
                child: Text('CREATE'),
              ),
              SizedBox(
                width: 30,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
