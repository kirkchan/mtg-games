import 'package:flutter/material.dart';

class LocationItem {
  const LocationItem({
    @required this.id,
    @required this.name,
    @required this.categories,
    @required this.cityDistrict,
    this.eventCount = 0,
    this.icon = Icons.category,
    this.iconColor = Colors.red,
  })  : assert(id != null),
        assert(name != null),
        assert(categories != null),
        assert(cityDistrict != null);

  final int id;
  final String name;
  final List categories;
  final String cityDistrict;
  final int eventCount;
  final IconData icon;
  final Color iconColor;

  @override
  String toString() => "$name (id=$id)";
}