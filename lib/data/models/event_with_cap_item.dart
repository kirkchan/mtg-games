import 'package:flutter/material.dart';
import 'package:mtg/data/models/event_item.dart';

class EventWithCapItem {

  final int id;
  final String name;
  final String format;
  final String status;
  final DateTime datetime;
  final int available;
  final int capacity;
  final Color color;

  EventWithCapItem(EventItem event, int capacity) :
    this.id = event.id,
    this.name = event.name,
    this.format = event.format,
    this.status = event.status,
    this.datetime = event.datetime,
    this.available = capacity,
    this.capacity = capacity,
    this.color = event.color;
}