import 'package:flutter/material.dart';
import 'package:mtg/data/models/event_item.dart';

class Location {
  const Location({
    @required this.id,
    @required this.name,
    @required this.categories,
    @required this.cityDistrict,
    this.eventList,
  })  : assert(id != null),
        assert(name != null),
        assert(categories != null),
        assert(cityDistrict != null);

  final int id;
  final String name;
  final List categories;
  final String cityDistrict;
  final List<EventItem> eventList;

  @override
  String toString() => "$name (id=$id)";
}
