import 'package:flutter/material.dart';

class EventItem {
  final int id;
  final String name;
  final String format;
  final String status;
  final DateTime datetime;
  final int available;
  final int capacity;
  final Color color;

  EventItem({
    @required this.id,
    @required this.name,
    @required this.format,
    @required this.status,
    @required this.datetime,
    this.available = 0,
    this.capacity = 0,
    this.color = Colors.grey,
  });

  @override
  String toString() => "$name (id=$id)";
}
