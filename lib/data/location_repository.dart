import 'dart:math';
import 'package:flutter/material.dart';
import 'package:mtg/data/models/event_item.dart';
import 'package:mtg/data/models/location.dart';

class LocationRepostiory {
  static final _colors = [
    Colors.red,
    Colors.orange,
    Colors.green,
    Colors.blue,
    Colors.purple,
  ];

  static Location loadLocation(int id) {
    List<EventItem> events = [
      EventItem(
        id: 1,
        name: 'Friday Night Magic',
        format: 'Standard',
        status: 'OPEN',
        datetime: DateTime(2020, 3, 6, 18, 30),
        capacity: 16,
        color: _generateColor(),
      ),
      EventItem(
        id: 2,
        name: 'Friday Night Magic',
        format: 'Limited Draft',
        status: 'OPEN',
        capacity: 8,
        datetime: DateTime(2020, 3, 6, 18, 30),
        color: _generateColor(),
      ),
      EventItem(
        id: 3,
        name: 'Friday Night Magic',
        format: 'Commander',
        status: 'OPEN',
        datetime: DateTime(2020, 2, 23, 12, 00),
        color: _generateColor(),
      ),
      EventItem(
        id: 4,
        name: 'Friday Night Magic',
        format: 'Commander',
        status: 'OPEN',
        datetime: DateTime(2020, 3, 8, 12, 00),
        color: _generateColor(),
      ),
      EventItem(
        id: 5,
        name: 'Friday Night Magic',
        format: 'Commander',
        status: 'OPEN',
        datetime: DateTime(2020, 3, 15, 12, 00),
        color: _generateColor(),
      ),
      EventItem(
        id: 6,
        name: 'Friday Night Magic',
        format: 'Commander',
        status: 'OPEN',
        datetime: DateTime(2020, 3, 22, 12, 00),
        color: _generateColor(),
      ),
    ];
    List<Location> locations = [
      Location(
        id: 22,
        name: 'The Iron Lion',
        cityDistrict: 'Colorado Springs, CO',
        categories: ['gaming', 'mtg'],
        eventList: events,
      ),
      Location(
        id: 33,
        name: 'Loyal Coffee',
        cityDistrict: 'Colorado Springs, CO',
        categories: ['coffee'],
        eventList: null
      ),
    ];

    return locations.where((Location l) {
      return l.id == id;
    }).toList()[0];
  }

  static Color _generateColor() {
    int i = (new Random().nextInt(5));
    return _colors.elementAt(i);
  }
}
