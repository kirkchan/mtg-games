import 'package:flutter/material.dart';
import 'package:mtg/data/models/location_item.dart';

enum Category { all, gaming, coffee, retail }

class SearchRepository {
  static List<LocationItem> loadResults(Category category) {
    const allLocations = <LocationItem>[
      LocationItem(
        id: 22,
        categories: [Category.gaming],
        name: 'Iron Lion Gaming',
        cityDistrict: 'Colorado Springs, CO',
      ),
      LocationItem(
        id: 33,
        categories: [Category.coffee],
        name: 'Loyal Coffee',
        cityDistrict: 'Colorado Springs, CO',
        icon: Icons.local_drink,
        iconColor: Colors.blue,
        eventCount: null,
      ),
      LocationItem(
        id: 22,
        categories: [Category.gaming],
        name: 'Iron Lion Gaming',
        cityDistrict: 'Colorado Springs, CO',
      ),
      LocationItem(
          id: 33,
          categories: [Category.coffee],
          name: 'Loyal Coffee',
          cityDistrict: 'Colorado Springs, CO',
          icon: Icons.local_drink,
          iconColor: Colors.blue),
      LocationItem(
        id: 22,
        categories: [Category.gaming],
        name: 'Iron Lion Gaming',
        cityDistrict: 'Colorado Springs, CO',
      ),
      LocationItem(
          id: 33,
          categories: [Category.coffee],
          name: 'Loyal Coffee',
          cityDistrict: 'Colorado Springs, CO',
          icon: Icons.local_drink,
          iconColor: Colors.blue),
      LocationItem(
        id: 22,
        categories: [Category.gaming],
        name: 'Iron Lion Gaming',
        cityDistrict: 'Colorado Springs, CO',
      ),
      LocationItem(
          id: 33,
          categories: [Category.coffee],
          name: 'Loyal Coffee',
          cityDistrict: 'Colorado Springs, CO',
          icon: Icons.local_drink,
          iconColor: Colors.blue),
      LocationItem(
        id: 22,
        categories: [Category.gaming],
        name: 'Iron Lion Gaming',
        cityDistrict: 'Colorado Springs, CO',
      ),
      LocationItem(
          id: 33,
          categories: [Category.coffee],
          name: 'Loyal Coffee',
          cityDistrict: 'Colorado Springs, CO',
          icon: Icons.local_drink,
          iconColor: Colors.blue),
      LocationItem(
        id: 22,
        categories: [Category.gaming],
        name: 'Iron Lion Gaming',
        cityDistrict: 'Colorado Springs, CO',
      ),
      LocationItem(
          id: 33,
          categories: [Category.coffee],
          name: 'Loyal Coffee',
          cityDistrict: 'Colorado Springs, CO',
          icon: Icons.local_drink,
          iconColor: Colors.blue),
    ];

    if (category == Category.all) {
      return allLocations;
    } else {
      return allLocations.where((LocationItem l) {
        return l.categories.contains(category);
      }).toList();
    }
  }
}
